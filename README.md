<h1 align=center>Presence API Demo Server</h1>

# Overview

If a vehicle gets recognized by LPR (License plate recognition) or another medium like NFC, 
our Presence API sends requests to a server. 

This repository serves as rudimentary example server that can receive those requests. 

You can use this example to build your own server, to which we send the results as JSON per HTTP POST.


# Configuration and monitoring

Login at https://monitor.arivo.app for configuration and monitoring.

## Configuration 

You can configure your server by following these steps:
 1. Click on the requested device
 ![](images/click_device.png)
 2. Click the `Edit Services` button
 ![](images/edit_services.png)
 3. Navigate to the `Vehicle Detection API Endpoint` service. If it does not exist, create it by pressing `+`
 ![](images/configure_service.png)
 4. Set `Server url` to the URL or IP of your server
 
 
You can optionally add an authentication method with username and password and customize which endpoints you want to use.


## Monitoring and health check

Navigate to the 'Edit Services' window mentioned at point 2. in [Configuration](#configuration). 

Click `Log` to check if the connection is working and to monitor the requests that are sent to your server.

![](images/log_viewer.png)


# Quickstart

* Install requirements with `pip install -r requirements.txt`
* Set username and password you chose in [Configuration](#configuration) in the [.env](.env) file for API authorization
* Run server with `bash start.sh`
* Open `http://localhost:8124/` in a browser


# Endpoints

There are 5 possible requests. They are sent in the following order per vehicle: 
  1. `/presence/enter` Gets sent once when vehicle enters the gate.
  2.  `/presence/change/` **OPTIONAL** Can be sent zero or multiple times, whenever vehicle data has changed. E.g. additional medium or changed license plate.
  3. `/presence/leave` Gets sent once when vehicle leaves the gate.   
  4. 
      *  `/presence/forward/` Gets sent once when the leaving direction is certain, **if** vehicle left going forward.  
        **OR**  
        * `/presence/backward/` Gets sent once when the leaving direction is certain, **if** vehicle left going backward.

Keep in mind:
  * Depending on the configuration of the lane, forward/backward either comes immediately after leave or a few seconds later.
  * In dense traffic it is possible that the enter of the next car comes before the forward/backward of the previous one. However a second enter cannot come before the leave of the previous one.

## Schema

The schema is hosted [here](https://arivo-public.gitlab.io/device-python/examples/api-presence-example-server/), 
or can be found at http://localhost:8124/docs when running the server locally. 

## Vehicle enters the gate

`POST /presence/enter/`

### Parameters
The fields of a [PresenceRequest](#presencerequest) object

### Returns
Returns a HTTP status of `200` on success

### Example

```json
{
  "vehicle": {
    "id": 16045067401,
    "lpr": {
      "id": "GARIVO1",
      "type": "lpr",
      "country": {
        "code": "A",
        "confidence": 1
      },
      "confidence": 1,
      "alternatives": null
    },
    "qr": null,
    "nfc": null,
    "pin": null 
  },
  "gateway": {
    "gate": "gate1",
    "direction": "in"
  }
}
```

## Vehicle leaves the gate

`POST /presence/leave/`

### Parameters
The fields of a [PresenceRequest](#presencerequest) object

### Returns
Returns a HTTP status of `200` if successful

### Example

```json
{
  "vehicle": {
    "id": 16045067401,
    "lpr": {
      "id": "GARIVO1",
      "type": "lpr",
      "country": {
        "code": "A",
        "confidence": 1
      },
      "confidence": 1,
      "alternatives": null
    },
    "qr": null,
    "nfc": null,
    "pin": null 
  },
  "gateway": {
    "gate": "gate1",
    "direction": "out"
  }
}
```

## Vehicle data has changed

`POST /presence/change/`

### Parameters
The fields of a [PresenceRequest](#presencerequest) object

### Returns
Returns a HTTP status of `200` if successful

### Example

```json
{
  "vehicle": {
    "id": 16045067401,
    "lpr": {
      "id": "GARIVO2",
      "type": "lpr",
      "country": {
        "code": "A",
        "confidence": 1
      },
      "confidence": 1,
      "alternatives": null
    },
    "qr": null,
    "nfc": null,
    "pin": null 
  },
  "gateway": {
    "gate": "gate1",
    "direction": "in"
  }
}
```

## Vehicle moves going forward (after leaving)

`POST /presence/forward/`

### Parameters
The fields of a [PresenceRequest](#presencerequest) object

### Returns
Returns a HTTP status of `200` if successful

### Example

```json
{
  "vehicle": {
    "id": 16045067401,
    "lpr": {
      "id": "GARIVO1",
      "type": "lpr",
      "country": {
        "code": "A",
        "confidence": 1
      },
      "confidence": 1,
      "alternatives": null
    },
    "qr": null,
    "nfc": null,
    "pin": null 
  },
  "gateway": {
    "gate": "gate1",
    "direction": "out"
  }
}
```

## Vehicle moves going backward (after leaving)

`POST /presence/backward/`

### Parameters
The fields of a [PresenceRequest](#presencerequest) object

### Returns
Returns a HTTP status of `200` if successful

### Example

```json
{
  "vehicle": {
    "id": 16045067401,
    "lpr": {
      "id": "GARIVO1",
      "type": "lpr",
      "country": {
        "code": "A",
        "confidence": 1
      },
      "confidence": 1,
      "alternatives": null
    },
    "qr": null,
    "nfc": null,
    "pin": null 
  },
  "gateway": {
    "gate": "gate1",
    "direction": "out"
  }
}
```

# Models

In the following description, if a field is not explicitly **REQUIRED**, it is considered OPTIONAL.

## PresenceRequest

A PresenceRequest gets generated, if a vehicle is detected at a gate. 

| **Field name** |          **Type**           | **Description**                                                                       |
| -------------- |:---------------------------:| --------------------------------------------------------------------------------------|
| vehicle        |     [Vehicle](#vehicle)     |**REQUIRED.** The detected vehicle.                                 |
| gate           |       [Gate](#gate)         |**REQUIRED.** The gate from where the request was sent.                                 |


### Gate

Gateway the user has to pass through for entry or exit.

| **Field name** |          **Type**           | **Description**                                      |
| -------------- |:---------------------------:| -----------------------------------------------------|
| gate        |          `string`       |**REQUIRED.**  The unique identifier of the gate.      |
| direction      |          `string`       |**REQUIRED.** Possible values:  `in`, `out` or empty string if unknown.  |


### Vehicle

The detected vehicle. 


| **Field name**    |            **Type**             | **Description**                                                          |
| ----------------- |:-------------------------------:| -------------------------------------------------------------------------|
| id                |           `integer`         | **REQUIRED** A unique id identifying the vehicle. Usually the timestamp of creation in nanoseconds.           |
| lpr               |     [LPRMedium](#lprmedium)     | Optional, set if vehicle was detected by License Plate Recognition.             |
| qr                |    [BaseMedium](#basemedium)    | Optional, set if vehicle was detected by QR code.             |
| nfc               |    [BaseMedium](#basemedium)    | Optional, set if vehicle was detected by NFC.             |
| pin               |    [BaseMedium](#basemedium)    | Optional, set if vehicle was detected by PIN.          |



#### LPRMedium

LPR Medium type for the vehicle

| **Field name** |            **Type**             | **Description**                                                          |
| -------------- |:-------------------------------:| -------------------------------------------------------------------------|
| id             |           `string`          |**REQUIRED.** The unique id, i.e. license plate.         |
| type           |           `string`          |**REQUIRED.** The type of the medium, in this case `lpr`.                      |
| country        |   [LPRCountry](#lprcountry)  |**REQUIRED.** Which country the vehicle is from, detected by LPR.          |
| confidence     |           `float`           |**REQUIRED.** Number between 0 and 1 determining the confidence of the recognition.                      |
| alternatives   |List [LPRAlternative](#lpralternative)|A list of possible alternatives, containing the license plate and respective confidence.                        |

##### LPRCountry

The detected country the vehicle is from

| **Field name** |            **Type**             | **Description**                                                          |
| -------------- |:-------------------------------:| -------------------------------------------------------------------------|
| code           |           `string`          |**REQUIRED.** The detected international license plate country code. See https://en.wikipedia.org/wiki/International_vehicle_registration_code        |
| state          |           `string`          |The optional district.         |
| confidence     |           `float`           |**REQUIRED.** Number between 0 and 1 determining the confidence of the recognition. Might be very low until license plate was read for some time.                      |

##### LPRAlternative

Alternative license plates with the respective confidence

| **Field name** |            **Type**             | **Description**                                                          |
| -------------- |:-------------------------------:| -------------------------------------------------------------------------|
| confidence     |           `float`           |**REQUIRED.** Number between 0 and 1 determining the confidence of the recognition.                      |
| plate          |           `string`           |**REQUIRED.** The recognized license plate.                    |

#### BaseMedium

Medium type for the vehicle

| **Field name** |            **Type**             | **Description**                                                          |
| -------------- |:-------------------------------:| -------------------------------------------------------------------------|
| id             |           `string`          |**REQUIRED.** The unique id, depending on type.        |
| type           |           `string`          |**REQUIRED.** The type of the medium, e.g. `qr`, `nfc`, `pin`.                      |
