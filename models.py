from enum import Enum
from typing import Optional, List

from pydantic import BaseModel, Field


class Direction(str, Enum):
    UNKNOWN = ""
    IN = "in"
    OUT = "out"


class Gate(BaseModel):
    gate: str = Field("", example="gate1", description="The unique identifier of the gate.")
    direction: Direction = Field("", example="in",
                                 description="Direction of gate. ```in```, ```out``` or empty string if unknown.")


class BaseMedium(BaseModel):
    id: str = Field(example="GARIVO1", description="The unique id, depending on type.")
    type: str = Field(example="qr", description="The type of the medium, e.g. ```qr```, ```nfc```, ```pin```")


class LPRAlternative(BaseModel):
    confidence: float = Field(example=1.0,
                              description="Number between 0 and 1 determining the confidence of the recognition.")
    plate: str = Field(example="GARIVO1", description="The recognized license plate.")


class LPRCountry(BaseModel):
    code: str = Field(example="A", description="The detected country code, e.g. ```A``` for Austria.")
    state: Optional[str] = Field(description="The optional district.")
    confidence: float = Field(1, example=1.0,
                              description="Number between 0 and 1 determining the confidence of the recognition.")


class LPRMedium(BaseMedium):
    id: str = Field(example="GARIVO1", description="The unique id, i.e. license plate.")
    type: str = Field("lpr", description="The type of the medium, in this case ```lpr```.")
    country: LPRCountry = Field(LPRCountry(code="OTHER"),
                                description="Which country the vehicle is from, detected by LPR.")
    confidence: float = Field(1, example=1.0,
                              description="Number between 0 and 1 determining the confidence of the recognition.")
    alternatives: Optional[List[LPRAlternative]] = Field(
        description="A list of possible alternatives, containing the license plate and respective confidence")


class Vehicle(BaseModel):
    id: int = Field(example=16045067401,
                    description="A unique id identifying the vehicle. Usually the timestamp of creation in nanoseconds.")
    lpr: Optional[LPRMedium] = Field(description="Set if vehicle was detected by License Plate Recognition.")
    qr: Optional[BaseMedium] = Field(description="Set if vehicle was detected by QR.")
    nfc: Optional[BaseMedium] = Field(description="Set if vehicle was detected by NFC.")
    pin: Optional[BaseMedium] = Field(description="Set if vehicle was detected by PIN.")


class PresenceRequest(BaseModel):
    vehicle: Vehicle = Field(description="The detected vehicle.")
    gateway: Gate = Field(description="The gate from where the request was sent.")
