import os
import secrets

from dotenv import load_dotenv
from fastapi import FastAPI, Depends, HTTPException, Request
from fastapi import status
from fastapi.responses import HTMLResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from models import PresenceRequest

load_dotenv()

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")
http_basic = HTTPBasic()

presence_requests = list()
current_vehicle = None


def authorize_basic(credentials: HTTPBasicCredentials = Depends(http_basic)):
    """
    Authorization via HTTPBasic.
    """

    correct_username = secrets.compare_digest(credentials.username, os.environ.get("user"))
    correct_password = secrets.compare_digest(credentials.password, os.environ.get("password"))
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect user or password",
            headers={"WWW-Authenticate": "Basic"},
        )


@app.get("/", response_class=HTMLResponse, include_in_schema=False)
def read_root(request: Request):
    """
    This endpoint only serves as a visualization for the checked in vehicles
    """
    context = {"request": request, "current_vehicle": current_vehicle, "presence_requests": presence_requests}
    return templates.TemplateResponse("index.html", context)


@app.post("/presence/enter/", dependencies=[Depends(authorize_basic)])
def enter(payload: PresenceRequest):
    """
    A vehicle is present

    Returns 200 on success
    """
    global current_vehicle
    current_vehicle = payload.vehicle

    presence_requests.append(payload)
    return status.HTTP_200_OK


@app.post("/presence/leave/", dependencies=[Depends(authorize_basic)])
def leave(payload: PresenceRequest):
    """
    A vehicle left

    Returns 200 on success
    """
    global current_vehicle
    current_vehicle = None

    presence_requests.append(payload)
    return status.HTTP_200_OK


@app.post("/presence/change/", dependencies=[Depends(authorize_basic)])
def change(payload: PresenceRequest):
    """
    Vehicle info has changed

    Returns 200 on success
    """
    global current_vehicle
    current_vehicle = payload.vehicle

    presence_requests.append(payload)
    return status.HTTP_200_OK


@app.post("/presence/forward/", dependencies=[Depends(authorize_basic)])
def forward(payload: PresenceRequest):
    """
    Can be sent after /presence/leave/.
    The vehicle left going forward (i.e. through the gate)

    Returns 200 on success
    """
    presence_requests.append(payload)
    return status.HTTP_200_OK


@app.post("/presence/backward/", dependencies=[Depends(authorize_basic)])
def backward(payload: PresenceRequest):
    """
    Can be sent after /presence/leave/.
    The vehicle left going backward (e.g. the gate didn't open)

    Returns 200 on success
    """
    presence_requests.append(payload)
    return status.HTTP_200_OK
